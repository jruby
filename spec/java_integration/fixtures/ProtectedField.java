package java_integration.fixtures;

public class ProtectedField {
  protected String strField = "1765";
  
  public String getStrField() {
    return strField;
  }
}